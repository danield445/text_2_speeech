from tkinter import*
from tkinter import filedialog as fd
from PIL import Image

c = fd.askopenfilename()
d = input("What extension: ")

img = Image.open(f"{c}")
e=fd.asksaveasfilename(defaultextension=f".{d}")
img.save(f"{e}")

print(f"file saved as new.{d}")
